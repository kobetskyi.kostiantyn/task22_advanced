'use strict';

window.addEventListener('DOMContentLoaded', () => {

    const rowsInput = document.querySelector('#row-input');
    const colsInput = document.querySelector('#col-input');
    const sbmtBtm = document.querySelector('#submit-btn');
    const tableContainer = document.querySelector('#table-container');
    const form = document.querySelector('#form');
    const validationMessage = document.querySelector('#validation');


    function setDefault() {
        rowsInput.value = 0;
        colsInput.value = 0;
    }
    setDefault();

    function validate(a, b) {
        if (isNaN(+a) || isNaN(+b)) {
            return 'Wrong input. Only numbers allowed';
        }
        if ((+a) < 0 || (+b) < 0) {
            return 'Wrong input. Only positive numbers allowed';
        }
        if ((+a) > 15 || (+b) > 15) {
            return "I can't create such big table. 15 is max";
        } else {
            return '';
        }
    }

    const createTable = () => {
        let rows = rowsInput.value;
        let cols = colsInput.value;

        let message = validate(rows, cols);
        if (message.length > 0) {
            validationMessage.innerText = message;
            setTimeout(() => {
                validationMessage.innerText = '';
            }, 3000);

            setDefault();
            return;
        } else {
            validationMessage.innerText = '';
            clearTable();
        }
        const table = document.createElement('table');

        for (let i = 0; i < cols; i++) {
            const col = document.createElement('col');
            col.setAttribute('width', '100');
            table.appendChild(col);
        }

        for (let i = 0; i < rows; i++) {
            const row = document.createElement('tr');
            for (let j = 0; j < cols; j++) {
                const cell = document.createElement('td');
                cell.innerText = `${i+1}${j+1}`;
                cell.classList.add('cell');
                row.appendChild(cell);
            }
            table.appendChild(row);
        }

        tableContainer.appendChild(table);
    };

    const clearTable = () => {
        tableContainer.innerHTML = '';
    };


    sbmtBtm.addEventListener('click', event => {
        event.preventDefault();
        createTable();
        setDefault();
    });

    function generateRandomColor() {
        let color = Math.floor(Math.random() * 16777215).toString(16);
        return `#${color}`;
    }

    function borderClicked(event) {
        let cell = document.querySelector('td');
        let cellHeight = cell.clientHeight;
        let cellWidth = cell.clientWidth;

        console.log('x' + event.offsetX);
        console.log('y' + event.offsetY);
        if (event.offsetY <= 0 || event.offsetY >= cellHeight) {
            console.log('border click Y');
            return true;
        }
        if (event.offsetX <= 0 || event.offsetX >= cellWidth) {
            console.log('border click X');
            return true;
        }
        return false;
    }

    tableContainer.addEventListener('click', (event) => {
        const target = event.target;
        if (target && target.classList.contains('cell') && !target.classList.contains('color') && !borderClicked(event)) {
            target.setAttribute('bgcolor', generateRandomColor());
            target.classList.add('color');
        } else if (target && target.classList.contains('cell') && target.classList.contains('color') && !borderClicked(event)) {
            target.setAttribute('bgcolor', 'white');
            target.classList.remove('color');
        }
    });
});